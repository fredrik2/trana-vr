AFRAME.registerComponent('enter-flat', {
  schema: {
    foo: {type: 'string'}
  },

  init: function () {
    var el = this.el;
    var data = this.data;

    var flat = document.querySelector('#the-flat');
    console.log(flat.getAttribute('position'))
    var flatPosition = [flat.getAttribute('position')['x'], flat.getAttribute('position')['y'], flat.getAttribute('position')['z']]
    var player1 = document.querySelector('#player1');
    


    console.log('Init enter flat', el, flat);
    console.log('Flat element is at', flatPosition);

    el.addEventListener('click',  function(event) {
      console.log('Got click event on appartment building ...');
      console.log('Going to ',flatPosition);
      function teleportIn(){
      player1.setAttribute('position', {x: flatPosition[0], y: flatPosition[1]+0.8, z: flatPosition[2]});
      player1.setAttribute("in_flat", "true");
      console.log("Listener:")
      console.log(player1.getAttribute("listener"))
      player1.getAttribute("listener").stepFactor = 0.005;

      console.log('Camera is now at', player1.getAttribute('position'));
    }
    setTimeout(teleportIn, 3000);

    });

  },

  update: function () {
    var el = this.el;
    var data = this.data;
  }
});
