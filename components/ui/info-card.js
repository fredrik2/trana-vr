AFRAME.registerComponent('info-card', {
  schema: {
    id: {type: 'string', default: 'building'},
    title: {type: 'string'},
    info: {type: 'string'}
  },

  init: function () {
    var data = this.data;
    var el = this.el;
    var camEl = document.querySelector('#player1');

    console.log('info card init.', data, el, camEl);

    var planeEl = document.createElement('a-plane');
    planeEl.setAttribute('id', 'info-card-plane-'+data.id);
    planeEl.setAttribute('position', '0.035 0.007 -0.03');
    planeEl.setAttribute('width', 0.02);
    planeEl.setAttribute('height', 0.01);
    planeEl.setAttribute('color', '#ffffff');
    planeEl.setAttribute('opacity', .9);
    planeEl.setAttribute('visible', false);
    camEl.appendChild(planeEl);

    var titleEl = document.createElement('a-text');
    titleEl.setAttribute('id', 'info-card-title-'+data.id);
    titleEl.setAttribute('position', '0.026 0.01 -0.03');
    titleEl.setAttribute('anchor', 'align');
    titleEl.setAttribute('baseline', 'top');
    titleEl.setAttribute('align', 'left');
    titleEl.setAttribute('value', data.title);
    titleEl.setAttribute('color', 'black');
    titleEl.setAttribute('width', 0.03);
    titleEl.setAttribute('visible', false);
    camEl.appendChild(titleEl);

    var infoEl = document.createElement('a-text');
    infoEl.setAttribute('id', 'info-card-info-'+data.id);
    infoEl.setAttribute('position', '0.026 0.008 -0.03');
    infoEl.setAttribute('anchor', 'align');
    infoEl.setAttribute('baseline', 'top');
    infoEl.setAttribute('align', 'left');
    infoEl.setAttribute('wrap-count', 36);
    infoEl.setAttribute('value', data.info);
    infoEl.setAttribute('color', 'black');
    infoEl.setAttribute('width', 0.015);
    infoEl.setAttribute('visible', false);
    camEl.appendChild(infoEl);

    el.addEventListener('mouseenter', function () {
      console.log('Mouse over info-card');
      planeEl.setAttribute('visible', true);
      titleEl.setAttribute('visible', true);
      infoEl.setAttribute('visible', true);
    });

    el.addEventListener('mouseleave', function () {
      console.log('Mouse leave info-card');
      planeEl.setAttribute('visible', false);
      titleEl.setAttribute('visible', false);
      infoEl.setAttribute('visible', false);
    });
  }
});
