@echo off
echo Uploading Components
aws s3 cp components s3://trana-vr.framtidsunderlag.se/components --recursive
echo.
echo Uploading Models
aws s3 cp models s3://trana-vr.framtidsunderlag.se/models --recursive
echo.
echo Uploading webpage
aws s3 cp index.html s3://trana-vr.framtidsunderlag.se
