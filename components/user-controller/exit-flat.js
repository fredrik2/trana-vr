AFRAME.registerComponent('exit-flat', {
  schema: {
    foo: {type: 'string'}
  },

  init: function () {
    var el = this.el;
    var data = this.data;

    var flat_door = document.querySelector('#flat-door');

    var player1 = document.querySelector('#player1');

    console.log('Init exit flat', el, flat_door);


    el.addEventListener('click',  function(event) {
      console.log('Got click event on appartment door ...');

      function teleportOut(){

      player1.setAttribute('position', {x: -7.921, y: 1.985, z: 13.014});
      player1.setAttribute('rotation', {x: 0, y: 90, z: 0});
      player1.setAttribute("in_flat", "false");

      console.log('Camera is now at', player1.getAttribute('position'));
      player1.getAttribute("listener").stepFactor = 0.05;
    }
    setTimeout(teleportOut, 2000);

    });

  },

  update: function () {
    var el = this.el;
    var data = this.data;
  }
});
