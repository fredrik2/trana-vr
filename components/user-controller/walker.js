
	var welcomeOn = true;


	AFRAME.registerComponent("listener", {
		schema :
		{
			stepFactor : {
				type : "number",
				default : 10000000000
			}
		},
		tick : function()

		{
			function vRadiansToDegrees(radians) {
      			return radians * 180 / Math.PI;
    		}
			function getVerticalAngle(cameraVector) {
            	return vRadiansToDegrees(Math.atan(cameraVector.y));
        	}

			var verticalAngle = getVerticalAngle(this.el.components.camera.camera.getWorldDirection());
        	//console.log('vertical angle:' + verticalAngle);

			if (verticalAngle < -20) {
				if (welcomeOn) {
					if (verticalAngle < -44.5){
					closeWelcome();
				}
				}

				worldDirection = this.el.components.camera.camera.getWorldDirection();
				x = worldDirection.x*2;
				y = 0;
				z = worldDirection.z*2;
				var a = new THREE.Vector3(x,y,z)
				this.el.components.camera.camera.parent.position.add((a).multiplyScalar(this.data.stepFactor));
		}}
	})


	function closeWelcome() {
		console.log('Walking while message is open ...');
		welcomeOn = false;
		var camEl = document.querySelector('#player1');
		var cursorEl = document.querySelector('#cursor');
		var infoEl = document.querySelector('#info-card-plane-welcome');
		var infoTitleEl = document.querySelector('#info-card-title-welcome');
		var infoInfoEl = document.querySelector('#info-card-info-welcome');

		cursorEl.removeAttribute('info-card');
		camEl.removeChild(infoEl);
		camEl.removeChild(infoTitleEl);
		camEl.removeChild(infoInfoEl);
	}




/*if (verticalAngle < -43) {
	camera.position.x += 0.15
      //move camera
    }*/
